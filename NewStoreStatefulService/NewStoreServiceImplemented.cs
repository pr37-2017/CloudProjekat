﻿using CommonLibrary;
using Microsoft.Azure;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Microsoft.ServiceFabric.Services.Communication.Wcf;
using Microsoft.ServiceFabric.Services.Communication.Wcf.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Fabric;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewStoreStatefulService
{
    public class NewStoreServiceImplemented : IStore

    {
        string InstanceId;
       // IReliableDictionary<string, int> ContainerCounter;
       // IReliableDictionary<string, Departure> ContainerDepartures;
        IReliableStateManager StateManager;
        public NewStoreServiceImplemented()
        {
            
        }

        public NewStoreServiceImplemented(IReliableStateManager stateManager, string id)
        {
            InstanceId = id;
            StateManager = stateManager;
        }

        public async void InitReliableCollection()
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");
            var ContainerDepartures = await this.StateManager.GetOrAddAsync<IReliableDictionary<string, Departure>>("ContainerDepartures");
            TableQuery<Departure> query = new TableQuery<Departure>();

            FabricClient fabricClient1 = new FabricClient();
            int partitionsNumber1 = (await fabricClient1.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/PubSubStatefulService"))).Count;
            var binding1 = WcfUtility.CreateTcpClientBinding();
            int index1 = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IPubSub>> servicePartitionClient1 = new ServicePartitionClient<WcfCommunicationClient<IPubSub>>(
                new WcfCommunicationClientFactory<IPubSub>(clientBinding: binding1),
                new Uri("fabric:/CIS_projekat/PubSubStatefulService"),
                new ServicePartitionKey(0));


            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                using (var tx = this.StateManager.CreateTransaction())
                {
                    //update if it has, add if it doesnt
                    var dataItem = await ContainerDepartures.TryGetValueAsync(tx, entity.Id);
                    if (dataItem.HasValue)
                    {
                        //and if value is different that before call pubsub
                        if (dataItem.Value.Price != entity.Price || dataItem.Value.Tickets != entity.Tickets || dataItem.Value.DateOfDeparture != entity.DateOfDeparture || dataItem.Value.DateOfReturn != entity.DateOfReturn || dataItem.Value.Destination != entity.Destination )
                        {

                            await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.ShouldRefresh());
                        }
                        await ContainerDepartures.SetAsync(tx, entity.Id, entity);
                    }
                    else
                    {
                        //not existing, call pubsub

                        await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.ShouldRefresh());
                        await ContainerDepartures.AddAsync(tx, entity.Id, entity);
                    }
                    // If an exception is thrown before calling CommitAsync, the transaction aborts, all changes are
                    // discarded, and nothing is saved to the secondary replicas.
                    await tx.CommitAsync();
                }
            }
           
        }
        public async Task<bool> AddNewDeparture(string destination, string types, double price, DateTime date1, DateTime date2, int tickets)
        {
           
            string id = Guid.NewGuid().ToString("N");
            Departure dept = new Departure(id, id, destination, types, price, date1, date2, tickets);
            var ContainerDepartures = await this.StateManager.GetOrAddAsync<IReliableDictionary<string, Departure>>("ContainerDepartures");
            using (var tx = this.StateManager.CreateTransaction())
            {

                await ContainerDepartures.AddAsync(tx, id, dept);
                // If an exception is thrown before calling CommitAsync, the transaction aborts, all changes are
                // discarded, and nothing is saved to the secondary replicas.
                await tx.CommitAsync();
            }

           
            return true;

        }

        public async Task<bool> AddNewDeparture(CancellationToken cancellationToken)
        {

            ServiceEventSource.Current.Message("I will now try to write to Store Table");
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("DepartureTableStorage");
                string id = Guid.NewGuid().ToString("N");
                var ContainerDepartures = await this.StateManager.GetOrAddAsync<IReliableDictionary<string, Departure>>("ContainerDepartures");
                TableQuery<Departure> query = new TableQuery<Departure>();
                //Dictionary<string, Departure> retval = new Dictionary<string, Departure>();
               // List<string> list1 = new List<string>();
               // List<string> list2 = new List<string>();
               // foreach (Departure entity in _table.ExecuteQuery(query))
               // {
                    // retval.Add(entity.Id, entity);
               //     list1.Add(entity.Id);
               // }
                using (var tx = this.StateManager.CreateTransaction())
                {
                    var enumerable = await ContainerDepartures.CreateEnumerableAsync(tx, key => key != "", EnumerationMode.Ordered);
                    var asyncEnumerator = enumerable.GetAsyncEnumerator();

                    while (await asyncEnumerator.MoveNextAsync(cancellationToken))
                    {
                      //  list2.Add(asyncEnumerator.Current.Value.Id);
                        // Process asyncEnumerator.Current.Key and asyncEnumerator.Current.Value as you wish
                        TableOperation insertOperation = TableOperation.InsertOrReplace(asyncEnumerator.Current.Value);
                        _table.Execute(insertOperation);
                    }
                }
               

                return true;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't write into the DepartureTable");
                return false;
            }

            return true;
        }



        public async Task<List<Departure>> GetAllDepartures()
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");

            TableQuery<Departure> query = new TableQuery<Departure>();
            List<Departure> retval = new List<Departure>();

            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                retval.Add(entity);
            }
            return retval;
        }

        public async Task<double> GetPrice(string departureId)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");

            TableQuery<Departure> query = new TableQuery<Departure>();
            List<Departure> retval = new List<Departure>();


            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                if (entity.Id == departureId)
                {
                    return entity.Price;
                }
            }
            return -1;
        }

        public async Task<bool> RollbackTickets(string departureId, int ticketNum)
        {
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("DepartureTableStorage");

                TableQuery<Departure> query = new TableQuery<Departure>();
                //List<Departure> retval = new List<Departure>();
                Departure d;

                foreach (Departure entity in _table.ExecuteQuery(query))
                {
                    if (entity.Id == departureId)
                    {
                        entity.Tickets = ticketNum;
                        TableOperation editOperation = TableOperation.Replace(entity);
                        _table.Execute(editOperation);
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't rollback to this amount of tickets");
                return false;
            }

        }

        public async Task<bool> CheckTickets(string departureId, int ticketNum)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");

            TableQuery<Departure> query = new TableQuery<Departure>();
            List<Departure> retval = new List<Departure>();


            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                if (entity.Id == departureId && entity.Tickets >= ticketNum)
                {
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> CommitTickets(string departureId, int ticketNum, string username)
        {
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("DepartureTableStorage");

                TableQuery<Departure> query = new TableQuery<Departure>();
                //List<Departure> retval = new List<Departure>();
                Departure d;

                CloudStorageAccount _storageAccount2;
                CloudTable _table2;
                string a2 = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount2 = CloudStorageAccount.Parse(a2);
                CloudTableClient tableClient2 = new CloudTableClient(new Uri(_storageAccount2.TableEndpoint.AbsoluteUri), _storageAccount2.Credentials);
                _table2 = tableClient.GetTableReference("PurchasedDepartures");
                string id2 = Guid.NewGuid().ToString("N");
                


                foreach (Departure entity in _table.ExecuteQuery(query))
                {
                    if (entity.Id == departureId && entity.Tickets >= ticketNum)
                    {
                        entity.Tickets -= ticketNum;
                        TableOperation editOperation = TableOperation.Replace(entity);
                        _table.Execute(editOperation);
                        TableOperation insertOperation = TableOperation.InsertOrReplace(new DeparturePurchased(id2, id2, departureId, username, ticketNum, DateTime.Now));
                        _table2.Execute(insertOperation);
                        return true;
                    }
                }

              
                return false;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't commit to this amount of tickets");
                return false;
            }

        }

        public async Task<string> GetBankAccount(string username)
        {
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("UserTableStorage");

                TableQuery<User> query = new TableQuery<User>();
                //List<Departure> retval = new List<Departure>();
                User d;
                string bankacc = "";
                foreach (User entity in _table.ExecuteQuery(query))
                {
                    if (entity.Username == username)
                    {
                        bankacc = entity.BankAccount;
                    }
                }
                return bankacc;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't find this bankaccount");
                return "";
            }
        }

        public async Task<int> GetRollbackTickets(string departureId)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");

            TableQuery<Departure> query = new TableQuery<Departure>();
            List<Departure> retval = new List<Departure>();


            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                if (entity.Id == departureId)
                {
                    return entity.Tickets;
                }
            }
            return -1;
        }

        public async Task<List<DeparturePurchased>> GetUsersDepartures(string username)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("PurchasedDepartures");

            TableQuery<DeparturePurchased> query = new TableQuery<DeparturePurchased>();
            List<DeparturePurchased> purchaseds = new List<DeparturePurchased>();


            foreach (DeparturePurchased entity in _table.ExecuteQuery(query))
            {
                if (entity.Username == username)
                {
                    purchaseds.Add(entity);
                }
            }

            return purchaseds;
           
        }

        public async Task<int> GetTicketsBought(string departureId)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("PurchasedDepartures");

            TableQuery<DeparturePurchased> query = new TableQuery<DeparturePurchased>();


            foreach (DeparturePurchased entity in _table.ExecuteQuery(query))
            {
                if (entity.DepartureId == departureId)
                {
                    return entity.Tickets;
                }
            }

            return -1;
        }

        public async Task<bool> ReturnPurchased(string departureId)
        {
            int tickets =  await GetTicketsBought(departureId);
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("DepartureTableStorage");

                TableQuery<Departure> query = new TableQuery<Departure>();
                //List<Departure> retval = new List<Departure>();
                Departure d;

                foreach (Departure entity in _table.ExecuteQuery(query))
                {
                    if (entity.Id == departureId)
                    {
                        entity.Tickets += tickets;
                        TableOperation editOperation = TableOperation.Replace(entity);
                        _table.Execute(editOperation);
                        //return true;
                    }
                }
               // return false;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't rollback to this amount of purchased tickets");
                return false;
            }

            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("PurchasedDepartures");

                TableQuery<DeparturePurchased> query = new TableQuery<DeparturePurchased>();
                //List<Departure> retval = new List<Departure>();
                DeparturePurchased d;

                foreach (DeparturePurchased entity in _table.ExecuteQuery(query))
                {
                    if (entity.DepartureId == departureId)
                    {
                        TableOperation deleteOperation = TableOperation.Delete(entity);
                        _table.Execute(deleteOperation);
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't delete from Purchased");
                return false;
            }
        }

        public async Task<List<Departure>> GetFilteredDepartures(string typeOf, DateTime date1, bool areThereTickets)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");

            TableQuery<Departure> query = new TableQuery<Departure>();
            List<Departure> purchaseds = new List<Departure>();


            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                if (typeOf != "" && entity.Type == typeOf)
                {
                    purchaseds.Add(entity);
                }

                if (date1 != null && date1 != DateTime.MinValue && entity.DateOfDeparture == date1)
                {
                    purchaseds.Add(entity);
                }
                if (entity.Tickets >= 0 && areThereTickets)
                {
                    purchaseds.Add(entity);
                }
            }

            return purchaseds;
        }
    }
}
