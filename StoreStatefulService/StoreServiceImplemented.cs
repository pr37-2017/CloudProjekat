﻿using CommonLibrary;
using Microsoft.Azure;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Microsoft.ServiceFabric.Services.Communication.Wcf;
using Microsoft.ServiceFabric.Services.Communication.Wcf.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Fabric;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreStatefulService
{
    public class StoreServiceImplemented : IStore

    {
        string InstanceId;
        IReliableDictionary<string, int> ContainerCounter;
        IReliableStateManager StateManager;
        public StoreServiceImplemented()
        {

        }

        public StoreServiceImplemented(IReliableStateManager stateManager, string id)
        {
            InstanceId = id;
            StateManager = stateManager;
        }
        public async Task<bool> AddNewDeparture(string destination, string types, double price, DateTime date1, DateTime date2, int tickets)
        {
            /*
            ServiceEventSource.Current.Message("I will now try to write to Store Table");
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("DepartureTableStorage");
                string id = Guid.NewGuid().ToString("N");
                TableOperation insertOperation = TableOperation.InsertOrReplace(new Departure("testkey",id,destination,types,price,date1,date2,tickets));
                _table.Execute(insertOperation);
                return true;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't write into the DepartureTable");
                return false;
            }
            */
            return true;
        }
    }
}
