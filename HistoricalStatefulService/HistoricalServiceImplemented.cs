﻿using CommonLibrary;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HistoricalStatefulService
{
    public class HistoricalServiceImplemented : IHistorical
    {

        string InstanceId;
        IReliableDictionary<string, int> ContainerCounter;
        IReliableStateManager StateManager;
        public HistoricalServiceImplemented()
        {

        }

        public HistoricalServiceImplemented(IReliableStateManager stateManager, string id)
        {
            InstanceId = id;
            StateManager = stateManager;
        }
        public async Task<List<Departure>> GetOldDepartures()
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("DepartureTableStorage");

            TableQuery<Departure> query = new TableQuery<Departure>();
            List<Departure> retval = new List<Departure>();

            foreach (Departure entity in _table.ExecuteQuery(query))
            {
                //ako je proslo 5 dana od datuma polaska
                if ((DateTime.Now - entity.DateOfDeparture).TotalDays > 5)
                {
                    retval.Add(entity);
                }
                
            }
            return retval;
        }
    }
}
