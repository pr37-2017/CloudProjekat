﻿using CommonLibrary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Microsoft.ServiceFabric.Services.Communication.Wcf;
using Microsoft.ServiceFabric.Services.Communication.Wcf.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Fabric;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        public string LoggedIN { get; set; }
        public HomeController()
        {
            ViewData["LoggedIn"] = false;
            ViewData["Filtered"] = "no";
            LoggedIN = "";
            //SubscribeToRefresh();
        }

        public  IActionResult Index()
        {
            // await GetData();
           // ViewData["Filtered"] = "no";
            return View();

        }
        [HttpGet]
        [Route("/HomeController/SubscribeToRefresh")]
        public async Task<JsonResult> SubscribeToRefresh()
        {
            //TODO vidi da li okida ovde uopste, u store okida razliku lepo
            bool refresh = false;

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/PubSubStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IPubSub>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IPubSub>>(
                new WcfCommunicationClientFactory<IPubSub>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/PubSubStatefulService"),
                new ServicePartitionKey(0));

            refresh = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetRefreshRequest());

           /* while (true)
            {
                if (refresh)
                {
                    return Json(true);
                }
                refresh = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetRefreshRequest());
                await Task.Delay(TimeSpan.FromSeconds(10));
            }
           */
            return Json(refresh);
        }

        public async Task<IActionResult> GetDataHTML()
        {
            List<Departure> source = await GetDataPrim();
            string result = "";
            foreach (Departure dr in source)
            {
                result += "<tr>" +
                "<td>" + dr.Destination + "</td>" +
                "<td>" + dr.Type + "</td>" +
                "<td>" + dr.Price + "</td>" +
                "<td>" + dr.DateOfDeparture + "</td>" +
                "<td>" + dr.DateOfReturn + "</td>" +
                "<td>" + dr.Tickets + "</td>" +
                "</tr>";
            }
            ViewData["DepartureTable"] = result;
            return View("Index");
        }

        /*
         <label for="destination">Add a new departure:</label><br>
            <input type="text" id="destination" name="destination" value="Belgrade"><br>
            <label for="types">Choose a type:</label>

            <select name="types" id="types">
                <option value="bus">Bus</option>
                <option value="train">Train</option>
                <option value="plane">Plane</option>
            </select>
            <label for="price">Price:</label><br>
            <input type="number" id="price" name="price" value="0"><br><br>
            <label for="date1">Date of departure:</label><br>
            <input type="date" id="date1" name="date1" value=""><br><br>
            <label for="date2">Date of return:</label><br>
            <input type="date" id="date2" name="date2" value=""><br><br>
            <label for="tickets">Number of tickets:</label><br>
            <input type="number" id="tickets" name="tickets" value="0"><br><br>
            <input type="submit" value="Submit">*/
        [HttpPost]
        [Route("/HomeController/PostData")]
        public async Task<IActionResult> PostData(string destination, string types, double price, DateTime date1, DateTime date2, int tickets)
        {
            
            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            bool a = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.AddNewDeparture(destination, types, price, date1, date2, tickets));
            //return View("Index");
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Route("/HomeController/FilterData")]
        public async Task<IActionResult> FilterData(string types,  DateTime date1,  bool hasTickets)
        {

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            List<Departure> retval = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetFilteredDepartures(types, date1, hasTickets));
            string result = "";
            foreach (Departure dr in retval)
            {
                result += "<tr>" +
                "<td>" + dr.Destination + "</td>" +
                "<td>" + dr.Type + "</td>" +
                "<td>" + dr.Price + "</td>" +
                "<td>" + dr.DateOfDeparture + "</td>" +
                "<td>" + dr.DateOfReturn + "</td>" +
                "<td>" + dr.Tickets + "</td>" +
                "</tr>";
            }
            ViewData["FilteredDepartures"] = result;
            ViewData["Filtered"] = "yes";
            // return RedirectToAction("Index");
            await GetWeatherData();
           // await GetUsersDepartures(username);
            await GetHistoricalData();
            return View("Index");
        }

        [HttpPost]
        [Route("/HomeController/Buy")]
        public async Task<IActionResult> Buy(string departureId, int ticketNum, string usernameBuy)
        {

            // string username = ViewData["LoggedIn"].ToString();
            string username = usernameBuy;

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/TransactionCoordinatorStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<ITransaction>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<ITransaction>>(
                new WcfCommunicationClientFactory<ITransaction>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/TransactionCoordinatorStatefulService"),
                new ServicePartitionKey(0));

            bool a = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Prepare(departureId,ticketNum,username));
            if (a)
            {
                bool b = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Commit(departureId, ticketNum, username));
                if (!b)
                {
                    await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Rollback(departureId, ticketNum, username));
                }
            }

            await GetData();
            await GetWeatherData();
            await GetUsersDepartures(username);
            await GetHistoricalData();
            ViewData["LoggedIn"] = usernameBuy;
            return View("Index");
        }

        [HttpPost]
        [Route("/HomeController/Cancel")]
        public async Task<IActionResult> Cancel(string departureId, string usernameCancel)
        {

            // string username = ViewData["LoggedIn"].ToString();
            string username = usernameCancel;

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/TransactionCoordinatorStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<ITransaction>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<ITransaction>>(
                new WcfCommunicationClientFactory<ITransaction>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/TransactionCoordinatorStatefulService"),
                new ServicePartitionKey(0));

            bool a = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Cancel(departureId, username));

            await GetData();
            await GetWeatherData();
            await GetUsersDepartures(username);
            await GetHistoricalData();
            ViewData["LoggedIn"] = usernameCancel;

            return View("Index");
        }


        [HttpGet]
        [Route("/HomeController/GetHistoricalData")]
        public async Task<JsonResult> GetHistoricalData()
        {
            
            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/HistoricalStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IHistorical>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IHistorical>>(
                new WcfCommunicationClientFactory<IHistorical>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/HistoricalStatefulService"),
                new ServicePartitionKey(0));

            List<Departure> retval = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetOldDepartures());

            

            string result = "";
            foreach (Departure dr in retval)
            {
                result += "<tr>" +
                "<td>" + dr.Destination + "</td>" +
                "<td>" + dr.Type + "</td>" +
                "<td>" + dr.Price + "</td>" +
                "<td>" + dr.DateOfDeparture + "</td>" +
                "<td>" + dr.DateOfReturn + "</td>" +
                "<td>" + dr.Tickets + "</td>" +
                "</tr>";
            }
            ViewData["OldDepartures"] = result;

            return Json(retval);
        }

        [HttpGet]
        [Route("/HomeController/GetData")]
        public async Task<JsonResult> GetData()
        {
            ViewData["Filtered"] = "no";

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            List<Departure> retval = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetAllDepartures());

            int val = 1;
            List<SelectListItem> optionIds = new List<SelectListItem>();
            foreach (Departure d in retval)
            {
                if ((DateTime.Now - d.DateOfDeparture).TotalDays < 5)
                {
                    optionIds.Add(new SelectListItem
                    {
                        Text = d.Id,
                        Value = d.Id
                    });
                    val++;
                }
            }

            ViewBag.departureId = optionIds;

            string result = "";
            foreach (Departure dr in retval)
            {
                
                result += "<tr>" +
                "<td>" + dr.Destination + "</td>" +
                "<td>" + dr.Type + "</td>" +
                "<td>" + dr.Price + "</td>" +
                "<td>" + dr.DateOfDeparture + "</td>" +
                "<td>" + dr.DateOfReturn + "</td>" +
                "<td>" + dr.Tickets + "</td>" +
                "</tr>";
            }
            ViewData["Departures"] = result;

            return Json(retval);
        }


        [HttpGet]
        [Route("/HomeController/GetUsersDepartures")]
        public async Task<bool> GetUsersDepartures(string username)
        {

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            List<DeparturePurchased> retval = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetUsersDepartures(username));

            int val = 1;
            List<SelectListItem> optionIds = new List<SelectListItem>();
            foreach (DeparturePurchased d in retval)
            {
                optionIds.Add(new SelectListItem
                {
                    Text = d.DepartureId,
                    Value = d.DepartureId
                });
                val++;
            }

            ViewBag.departurePurchasedId = optionIds;

            string result = "";
            foreach (DeparturePurchased dr in retval)
            {
                result += "<tr>" +
                "<td>" + dr.DepartureId + "</td>" +
                "<td>" + dr.Tickets + "</td>" +
                "<td>" + dr.DatePurchased + "</td>" +
                "</tr>";
            }
            ViewData["UsersDepartures"] = result;
            ViewData["LoggedIn"] = username;
            //return Json(retval);
            return true;
        }

        public async Task<List<Departure>> GetDataPrim()
        {

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            List<Departure> retval = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetAllDepartures());
            return retval;
        }

        [HttpGet]
        [Route("/HomeController/GetWeatherData")]
        public async Task<JsonResult> GetWeatherData()
        {
            List<Departure> departures = await GetDataPrim();
            
            var myBinding = new NetTcpBinding(SecurityMode.None);
           // myBinding.MaxReceivedMessageSize = 2147483647;
            var myEndpoint = new EndpointAddress("net.tcp://localhost:53851/WeatherEndpoint");
            string retval;
            List<Weather> w = new List<Weather>();
            foreach(Departure d in departures)
            using (var myChannelFactory = new ChannelFactory<IWeather>(myBinding, myEndpoint))
            {
                IWeather client = null;
               
                try
                {
                    client = myChannelFactory.CreateChannel();

                     retval = await client.GetTheWether(d.Destination);
                        w.Add(new Weather(retval, d.Destination));
                    ((ICommunicationObject)client).Close();
                    myChannelFactory.Close();
                  //  return Json(w);
                }
                catch
                {
                    (client as ICommunicationObject)?.Abort();
                }
            }

            string result = "";
            foreach (Weather dr in w)
            {
                result += "<tr>" +
                "<td>" + dr.Destination + "</td>" +
                "<td>" + dr.WeatherString + "</td>" +
                "</tr>";
            }
            ViewData["Weather"] = result;

            return Json(w);
        }

        [HttpPost]
        [Route("/HomeController/Login")]
        public async Task<IActionResult> Login(string username, string password)
        {

            var myBinding = new NetTcpBinding(SecurityMode.None);
            var myEndpoint = new EndpointAddress("net.tcp://localhost:53853/UserEndpoint");
            bool retval = false;
            using (var myChannelFactory = new ChannelFactory<IAuthenticate>(myBinding, myEndpoint))
            {
                IAuthenticate client = null;

                try
                {
                    client = myChannelFactory.CreateChannel();

                    retval = await client.LogIn(username, password);
                    ((ICommunicationObject)client).Close();
                    myChannelFactory.Close();
                }
                catch
                {
                    (client as ICommunicationObject)?.Abort();
                    ViewData["LoggedIn"] = "";
                }

                ViewData["LoggedIn"] = username;
                LoggedIN = username;
                await GetData();
                await GetWeatherData();
                await GetUsersDepartures(username);
                await GetHistoricalData();
                return View("Index"); 
            }
        }

        [HttpPost]
        [Route("/HomeController/Register")]
        public async Task<IActionResult> Register(string username, string email, string password, string bankaccount)
        {
            var myBinding = new NetTcpBinding(SecurityMode.None);
            var myEndpoint = new EndpointAddress("net.tcp://localhost:53853/UserEndpoint");
            bool retval = false;
            using (var myChannelFactory = new ChannelFactory<IAuthenticate>(myBinding, myEndpoint))
            {
                IAuthenticate client = null;

                try
                {
                    client = myChannelFactory.CreateChannel();

                    retval = await client.Register(username, email, password, bankaccount);
                    ((ICommunicationObject)client).Close();
                    myChannelFactory.Close();
                    //  return Json(w);
                }
                catch
                {
                    (client as ICommunicationObject)?.Abort();
                }

                await GetData();
                await GetWeatherData();
                await GetHistoricalData();
                return RedirectToAction("Index");
            }
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
