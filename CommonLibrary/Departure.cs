﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class Departure : TableEntity
    {
        public Departure()
        {
        }

        public Departure(string rowKey, string id, string dest, string type, double price, DateTime ddept, DateTime dret, int tickets)
        {
            PartitionKey = "DepartureTableStorage";
            RowKey = rowKey;
            Id = id;
            Destination = dest;
            Type = type;
            Price = price;
            DateOfDeparture = ddept;
            DateOfReturn = dret;
            Tickets = tickets;
        }

        public string Id { get; set; }
        public string Destination { get; set; }
        public string Type { get; set; }
        public double Price { get; set; }
        public DateTime DateOfDeparture { get; set; }
        public DateTime DateOfReturn { get; set; }
        public int Tickets { get; set; }

    }
}
