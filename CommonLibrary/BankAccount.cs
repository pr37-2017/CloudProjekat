﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class BankAccount : TableEntity
    {
        public BankAccount()
        {
        }

        public BankAccount(string partitionKey, string rowKey, string acc, double bal) : base(partitionKey, rowKey)
        {
            AccountNumber = acc;
            Balance = bal;
        }

        public string AccountNumber { get; set; }
        public double Balance { get; set; }

    }
}

