﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    [ServiceContract]
    public interface IAuthenticate
    {
        [OperationContract]
        Task<bool> LogIn(string username, string password);

        [OperationContract]
        Task<bool> Register(string username, string email, string password, string bankaccount);
    }
}
