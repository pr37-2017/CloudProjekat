﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class User : TableEntity
    {
        public User()
        {
        }

        public User(string partitionKey, string rowKey, string u, string e, string p, string b) : base(partitionKey, rowKey)
        {
            PartitionKey = partitionKey;
            RowKey = rowKey;
            Username = u;
            Email = e;
            Password = p;
            BankAccount = b;
            Departures = new Dictionary<string, int>();
        }

        public override IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            var results = base.WriteEntity(operationContext);
            foreach (var item in Departures)
            {
                results.Add("D_" + item.Key, new EntityProperty(item.Value));
            }
            return results;
        }

        public override void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            base.ReadEntity(properties, operationContext);

            Departures = new Dictionary<string, int>();

            foreach (var item in properties)
            {
                if (item.Key.StartsWith("D_"))
                {
                    string realKey = item.Key.Substring(2);
                    Departures[realKey] = int.Parse(item.Value.StringValue);
                }
            }
        }

        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Dictionary<string, int> Departures { get; set; } //list of all departures ids, num of tickets
        public string BankAccount { get; set; }
    }
}
