﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    [ServiceContract]
    public interface IStore
    {
        [OperationContract]
        Task<bool> AddNewDeparture(string destination, string types, double price, DateTime date1, DateTime date2, int tickets);

        [OperationContract]
        Task<List<Departure>> GetAllDepartures();

        [OperationContract]
        Task<List<DeparturePurchased>> GetUsersDepartures(string username);
        [OperationContract]
        Task<List<Departure>> GetFilteredDepartures(string typeOf, DateTime date1, bool areThereTickets);

        [OperationContract]
        Task<double> GetPrice(string departureId);
        [OperationContract]
        Task<bool> CheckTickets(string departureId, int ticketNum);
        [OperationContract]
        Task<bool> CommitTickets(string departureId, int ticketNum, string username);
        [OperationContract]
        Task<bool> RollbackTickets(string departureId, int ticketNum);
        [OperationContract]
        Task<string> GetBankAccount(string username);
        [OperationContract]
        Task<int> GetRollbackTickets(string departureId);
        [OperationContract]
        Task<int> GetTicketsBought(string departureId);
        [OperationContract]
        Task<bool> ReturnPurchased(string departureId);
    }
}
