﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    [ServiceContract]
    public interface IBank
    {
        [OperationContract]
        Task<bool> CheckBalance(string bankaccount, double amount);
        [OperationContract]
        Task<bool> TakeAmount(string bankaccount, double amount);
        [OperationContract]
        Task<bool> ReturnAmount(string bankaccount, double amount);
        [OperationContract]
        Task<double> GetRollbackAmount(string bankaccount);
    }
}
