﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    [ServiceContract]
    public interface IPubSub
    {
        //if new data is added send a refresh page request
        [OperationContract]
        Task<bool> ShouldRefresh();
        [OperationContract]
        Task<bool> GetRefreshRequest();
    }
}
