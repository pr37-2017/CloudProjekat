﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class Weather 
    {
        public Weather(string weatherString, string destination)
        {
            WeatherString = weatherString;
            Destination = destination;
        }

        public string WeatherString { get; set; }
        public string Destination { get; set; }
    }
}
