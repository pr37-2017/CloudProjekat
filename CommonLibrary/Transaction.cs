﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class Transaction
    {
        public string Id { get; set; }

        public string IdOfDeparture { get; set; }
        public int BoughtTickets { get; set; }
    }
}
