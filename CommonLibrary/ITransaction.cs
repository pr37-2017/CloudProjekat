﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    [ServiceContract]
    public interface ITransaction
    {
        [OperationContract]
        Task<bool> Prepare(string departureId, int ticketNum, string bankaccount);
        [OperationContract]
        Task<bool> Commit(string departureId, int ticketNum, string bankaccount);
        [OperationContract]
        Task<bool> Rollback(string departureId, int ticketNum, string bankaccount);
        [OperationContract]
        Task<bool> Cancel(string departureId, string username);
    }
}
