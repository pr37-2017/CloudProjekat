﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonLibrary
{
    public class DeparturePurchased : TableEntity
    {
        public DeparturePurchased()
        {
        }

        public DeparturePurchased(string partitionKey, string rowKey, string deptid, string usern, int tick, DateTime purch) : base(partitionKey, rowKey)
        {
            DepartureId = deptid;
            Username = usern;
            Tickets = tick;
            DatePurchased = purch;
        }

        public string DepartureId { get; set; }
        public string Username { get; set; }
        public int Tickets { get; set; }
        public DateTime DatePurchased { get; set; }
    }
}
