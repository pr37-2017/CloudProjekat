﻿using CommonLibrary;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserStatelessService
{
    public class UserServiceImplemented : IAuthenticate
    {
        public async Task<bool> LogIn(string username, string password)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("UserTableStorage");

            TableQuery<User> query = new TableQuery<User>();
            // List<User> retval = new List<User>();

            foreach (User entity in _table.ExecuteQuery(query))
            {
                if (entity.Username == username && entity.Password == password)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> Register(string username, string email, string password, string bankaccount)
        {
            ServiceEventSource.Current.Message("I will now try to write to User Table");
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("UserTableStorage");
                string id = Guid.NewGuid().ToString("N");
                TableOperation insertOperation = TableOperation.InsertOrReplace(new User("userpartition", id, username, email, password, bankaccount));
                _table.Execute(insertOperation);
                return true;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't write into the UserTable");
                return false;
            }
        }
    }
}
