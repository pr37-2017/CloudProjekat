﻿using CommonLibrary;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Data.Collections;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankStatefulService
{
    public class BankServiceImplemented : IBank
    {

        string InstanceId;
        IReliableDictionary<string, int> ContainerCounter;
        IReliableStateManager StateManager;
        public BankServiceImplemented()
        {

        }

        public BankServiceImplemented(IReliableStateManager stateManager, string id)
        {
            InstanceId = id;
            StateManager = stateManager;
        }
        public async Task<bool> CheckBalance(string bankaccount, double amount)
        {
            CloudStorageAccount _storageAccount;
            CloudTable _table;
            string a = ConfigurationManager.AppSettings["DataConnectionString"];
            _storageAccount = CloudStorageAccount.Parse(a);
            CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
            _table = tableClient.GetTableReference("BankTableStorage");

            TableQuery<BankAccount> query = new TableQuery<BankAccount>();


            foreach (BankAccount entity in _table.ExecuteQuery(query))
            {
                if (entity.AccountNumber == bankaccount && entity.Balance >= amount)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<double> GetRollbackAmount(string bankaccount)
        {
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("BankTableStorage");

                TableQuery<BankAccount> query = new TableQuery<BankAccount>();

                foreach (BankAccount entity in _table.ExecuteQuery(query))
                {
                    if (entity.AccountNumber == bankaccount)
                    {
                        return entity.Balance;
                    }
                }
                return -1;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't get into the BankTable");
                return -1;
            }
        }

        public async Task<bool> ReturnAmount(string bankaccount, double amount)
        {
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("BankTableStorage");

                TableQuery<BankAccount> query = new TableQuery<BankAccount>();

                foreach (BankAccount entity in _table.ExecuteQuery(query))
                {
                    if (entity.AccountNumber == bankaccount)
                    {
                        entity.Balance += amount;
                        TableOperation editOperation = TableOperation.Replace(entity);
                        _table.Execute(editOperation);
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't edit into the BankTable");
                return false;
            }
        }

        public async Task<bool> TakeAmount(string bankaccount, double amount)
        {
            try
            {
                CloudStorageAccount _storageAccount;
                CloudTable _table;
                string a = ConfigurationManager.AppSettings["DataConnectionString"];
                _storageAccount = CloudStorageAccount.Parse(a);
                CloudTableClient tableClient = new CloudTableClient(new Uri(_storageAccount.TableEndpoint.AbsoluteUri), _storageAccount.Credentials);
                _table = tableClient.GetTableReference("BankTableStorage");

                TableQuery<BankAccount> query = new TableQuery<BankAccount>();

                foreach (BankAccount entity in _table.ExecuteQuery(query))
                {
                    if (entity.AccountNumber == bankaccount && entity.Balance >= amount)
                    {
                        entity.Balance -= amount;
                        TableOperation editOperation = TableOperation.Replace(entity);
                        _table.Execute(editOperation);
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                ServiceEventSource.Current.Message("I can't edit into the BankTable");
                return false;
            }
        }
    
    }
}
