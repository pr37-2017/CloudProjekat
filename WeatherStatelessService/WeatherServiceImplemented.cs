﻿using CommonLibrary;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStatelessService
{
    public class WeatherServiceImplemented : IWeather
    {
        public async Task<string> GetTheWether(string city)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://api.openweathermap.org");
                    var response = await client.GetAsync($"/data/2.5/weather?q={city}&appid=812e8bcec326b73e824cffdf7a9f70fc&units=metric");
                    response.EnsureSuccessStatusCode();

                    var stringResult = await response.Content.ReadAsStringAsync();
                    var rawWeather = JsonConvert.DeserializeObject<OpenWeatherResponse>(stringResult);

                    string Temp = rawWeather.Main.Temp;
                    string Summary = string.Join(",", rawWeather.Weather.Select(x => x.Main));
                    string retval = Temp + "C" + "; " + Summary;
                    return retval;

                }
                catch (HttpRequestException httpRequestException)
                {
                    return "Error getting weather from OpenWeather";
                }
            }
        }
    }
}
