﻿using CommonLibrary;
using Microsoft.ServiceFabric.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubSubStatefulService
{
    
    public class PubSubServiceImplemented : IPubSub
    {
        string InstanceId;
        // IReliableDictionary<string, int> ContainerCounter;
        // IReliableDictionary<string, Departure> ContainerDepartures;
        IReliableStateManager StateManager;
        bool sendRefreshRequest;
        public PubSubServiceImplemented()
        {
            sendRefreshRequest = false;
        }

        public PubSubServiceImplemented(IReliableStateManager stateManager, string id)
        {
            InstanceId = id;
            StateManager = stateManager;
            sendRefreshRequest = false;
        }

        public async Task<bool> GetRefreshRequest()
        {
            if (sendRefreshRequest)
            {
                sendRefreshRequest = false;
                return true;
            }
            return false;
        }

        public async Task<bool> ShouldRefresh()
        {
            sendRefreshRequest = true;
            return true;
        }
    }
}
