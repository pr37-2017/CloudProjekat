﻿using System;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLibrary;
using Microsoft.ServiceFabric.Data;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Microsoft.ServiceFabric.Services.Communication.Wcf;
using Microsoft.ServiceFabric.Services.Communication.Wcf.Client;

namespace TransactionCoordinatorStatefulService
{
   public class TransactionCoordinatorImplemented : CommonLibrary.ITransaction
    {
        string InstanceId;
        IReliableStateManager StateManager;
        public int rollbackDeptTickets { get; set; }
        public double rollbackBankAmount { get; set; }
        public TransactionCoordinatorImplemented()
        {

        }

        public TransactionCoordinatorImplemented(IReliableStateManager stateManager, string id)
        {
            InstanceId = id;
            StateManager = stateManager;
        }
        public async Task<bool> Commit(string departureId, int ticketNum, string username)
        {
            double amount = 0;

            FabricClient fabricClient1 = new FabricClient();
            int partitionsNumber1 = (await fabricClient1.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding1 = WcfUtility.CreateTcpClientBinding();
            int index1 = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient1 = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding1),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            double price = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetPrice(departureId));
            this.rollbackBankAmount = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetRollbackTickets(departureId));
            bool retvalStore = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.CommitTickets(departureId, ticketNum, username));
            string bankaccount = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetBankAccount(username));
            
            if (!retvalStore)
            {
                return retvalStore;
            }
            amount = ticketNum * price;

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/BankStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IBank>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IBank>>(
                new WcfCommunicationClientFactory<IBank>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/BankStatefulService"),
                new ServicePartitionKey(0));

            this.rollbackBankAmount = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.GetRollbackAmount(bankaccount));
            bool retvalBank = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.TakeAmount(bankaccount, amount));
            
            return (retvalBank && retvalStore);
        }

        public async Task<bool> Prepare(string departureId, int ticketNum, string username)
        {
            double amount = 0;

            FabricClient fabricClient1 = new FabricClient();
            int partitionsNumber1 = (await fabricClient1.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding1 = WcfUtility.CreateTcpClientBinding();
            int index1 = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient1 = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding1),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            double price = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetPrice(departureId));
            bool retvalStore = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.CheckTickets(departureId, ticketNum));
            string bankaccount = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetBankAccount(username));
            if (!retvalStore)
            {
                return retvalStore;
            }
            amount = ticketNum * price;

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/BankStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IBank>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IBank>>(
                new WcfCommunicationClientFactory<IBank>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/BankStatefulService"),
                new ServicePartitionKey(0));

            bool retvalBank = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.CheckBalance(bankaccount, amount));
            return (retvalBank && retvalStore);
        }

        public async Task<bool> Rollback(string departureId, int ticketNum, string username)
        {
            FabricClient fabricClient1 = new FabricClient();
            int partitionsNumber1 = (await fabricClient1.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding1 = WcfUtility.CreateTcpClientBinding();
            int index1 = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient1 = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding1),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            bool retvalStore = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.RollbackTickets(departureId, this.rollbackDeptTickets));
            string bankaccount = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetBankAccount(username));
            if (!retvalStore)
            {
                return retvalStore;
            }

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/BankStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IBank>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IBank>>(
                new WcfCommunicationClientFactory<IBank>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/BankStatefulService"),
                new ServicePartitionKey(0));

            bool retvalBank = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.ReturnAmount(bankaccount, this.rollbackBankAmount));
            return (retvalBank && retvalStore);
        }

        public async Task<bool> Cancel(string departureId, string username)
        {
            FabricClient fabricClient1 = new FabricClient();
            int partitionsNumber1 = (await fabricClient1.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/NewStoreStatefulService"))).Count;
            var binding1 = WcfUtility.CreateTcpClientBinding();
            int index1 = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IStore>> servicePartitionClient1 = new ServicePartitionClient<WcfCommunicationClient<IStore>>(
                new WcfCommunicationClientFactory<IStore>(clientBinding: binding1),
                new Uri("fabric:/CIS_projekat/NewStoreStatefulService"),
                new ServicePartitionKey(0));

            double price = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetPrice(departureId));
            double tickets = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetTicketsBought(departureId));
            bool retvalStore = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.ReturnPurchased(departureId));
            string bankaccount = await servicePartitionClient1.InvokeWithRetryAsync(client => client.Channel.GetBankAccount(username));
            if (!retvalStore)
            {
                return retvalStore;
            }

            double amount = price * tickets;

            FabricClient fabricClient = new FabricClient();
            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/BankStatefulService"))).Count;
            var binding = WcfUtility.CreateTcpClientBinding();
            int index = 0;
            //for (int i = 0; i < partitionsNumber; i++)
            //{
            ServicePartitionClient<WcfCommunicationClient<IBank>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<IBank>>(
                new WcfCommunicationClientFactory<IBank>(clientBinding: binding),
                new Uri("fabric:/CIS_projekat/BankStatefulService"),
                new ServicePartitionKey(0));

            bool retvalBank = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.ReturnAmount(bankaccount, amount));
            return (retvalBank && retvalStore);
        }
    }
}
