﻿using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using System;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Limilabs.Mail;
using Limilabs.Client.IMAP;
using Microsoft.ServiceFabric.Services.Communication.Wcf;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Microsoft.ServiceFabric.Services.Communication.Wcf.Client;
using CommonLibrary;
using Microsoft.ServiceFabric.Services.Client;

namespace EmailStatelessService
{
    /// <summary>
    /// An instance of this class is created for each service instance by the Service Fabric runtime.
    /// </summary>
    internal sealed class EmailStatelessService : StatelessService
    {
        Imap imap;
        public EmailStatelessService(StatelessServiceContext context)
            : base(context)
        {
          

        }

        /// <summary>
        /// Optional override to create listeners (e.g., TCP, HTTP) for this service replica to handle client or user requests.
        /// </summary>
        /// <returns>A collection of listeners.</returns>
        protected override IEnumerable<ServiceInstanceListener> CreateServiceInstanceListeners()
        {
            return new ServiceInstanceListener[0];
        }

        /// <summary>
        /// This is the main entry point for your service instance.
        /// </summary>
        /// <param name="cancellationToken">Canceled when Service Fabric needs to shut down this service instance.</param>
        protected override async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following sample code with your own logic 
            //       or remove this RunAsync override if it's not needed in your service.

            long iterations = 0;

            imap = new Imap();
            imap.SSLConfiguration.EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12;
            imap.ConnectSSL("imap.gmail.com", 993);
            imap.UseBestLogin("t.dragana.pr37@gmail.com", "adcdidvzjixkbofd");
            imap.SelectInbox();

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();

                try
                {
                    

                    List<long> uids = imap.Search(Flag.Unseen);

                    foreach (long uid in uids)
                    {
                        var eml = imap.GetMessageByUID(uid);
                        IMail email = new MailBuilder()
                        .CreateFromEml(eml);

                        // Console.WriteLine(email.Subject);
                        // Console.WriteLine(email.Text);
                        //if (email.Subject == "BUY")
                        //{
                        //    ServiceEventSource.Current.Message("EMAIL:" + email.Text);
                        //}
                        List<string> result = email.Text?.Split(',').ToList();
                        if (result[0] == "BUY")
                        {
                            string departureId = result[1];
                            int ticketNum = Int32.Parse(result[2]);
                            string username = result[3];

                            FabricClient fabricClient = new FabricClient();
                            int partitionsNumber = (await fabricClient.QueryManager.GetPartitionListAsync(new Uri("fabric:/CIS_projekat/TransactionCoordinatorStatefulService"))).Count;
                            var binding = WcfUtility.CreateTcpClientBinding();
                            int index = 0;
                            //for (int i = 0; i < partitionsNumber; i++)
                            //{
                            ServicePartitionClient<WcfCommunicationClient<ITransaction>> servicePartitionClient = new ServicePartitionClient<WcfCommunicationClient<ITransaction>>(
                                new WcfCommunicationClientFactory<ITransaction>(clientBinding: binding),
                                new Uri("fabric:/CIS_projekat/TransactionCoordinatorStatefulService"),
                                new ServicePartitionKey(0));

                            bool a = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Prepare(departureId, ticketNum, username));
                            if (a)
                            {
                                bool b = await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Commit(departureId, ticketNum, username));
                                if (!b)
                                {
                                    await servicePartitionClient.InvokeWithRetryAsync(client => client.Channel.Rollback(departureId, ticketNum, username));
                                }
                            }
                        }
                    }
                    uids.Clear();
                }
                catch (Exception e)
                {
                    ServiceEventSource.Current.Message("EMAIL:" + e.Message);
                }

                ServiceEventSource.Current.ServiceMessage(this.Context, "Working EMAIL-{0}", ++iterations);

                await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);
            }
        }
    }
}
